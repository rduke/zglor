#ifndef __ZGLOR_GL_FONT_RENDERER_H__
#define __ZGLOR_GL_FONT_RENDERER_H__

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <GL>

namespace zglor
{

	class GLFontRenderer
	{
		
	private:

		static bool m_isInit;
		static unsigned int m_textureObject;
		static int m_screenWidth;
		static int m_screenHeight;
		static float m_color[ 4 ];

	public:
		
		static bool init();
		static void print( float x,
						   float y,
						   float fontSize,
						   const char* pString,
						   bool forceMonoSpace = false,
						   int monoSpaceWidth = 11,
						   bool doOrthoProj = true );

		static void setScreenResolution( int screenWidth, int screenHeight );
	};

}

#endif // __ZGLOR_GL_FONT_RENDERER_H__