#ifndef __ZGLOR_ORTHOGRAPHIC_DRAWING_H__
#define __ZGLOR_ORTHOGRAPHIC_DRAWING_H__

#include <GL>
#include <NxFoundation.h>
#include "zglorGLFontRenderer.h"

namespace zglor
{

	class OrthographicDrawing
	{
	public:
		void drawCircle( float posX,
						 float posY,
						 float radius,
						 float fromDegree = 0,
						 float toDegree = 360,
						 float stepDegree = 10,
						 int bigMarks = 0,
						 int smallMarks = 0 );

		void drawLine( float x1,
					   float y1,
					   float x2,
					   float y2 );

		void drawText( int xpos,
					   int ypos,
					   const char* pText,
					   int _width,
					   int _height );

		float mWidth, mHeight;

		void setOrthographicProjection( float w, float h );
		void resetPerspectiveProjection();

	};

}

#endif //__ZGLOR_ORTHOGRAPHIC_DRAWING_H__
