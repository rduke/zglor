#ifndef __ZGLOR_OBJECT_RENDERER_H__
#define __ZGLOR_OBJECT_RENDERER_H__

#include "NxPhysics.h"
#include <GL>
#include "zglorTerrainData.h"
#include "zglorSingleton.h"

#define SHAPE_DEFAULT_COLOR NxVec3(0.7f, 0.7f, 0.7f)

namespace zglor
{

	typedef NxVec3 Point;
	typedef struct _Triangle { NxU32 p0;
							   NxU32 p1;
							   NxU32 p2;
							 } Triangle;

	class ObjectRenderer
		: public Singleton< ObjectRenderer >
	{
	public:
		void setupGLMatrix( const NxVec3& pos, 
							const NxMat33& orient );

		void drawLine( const NxVec3& p0,
					   const NxVec3& p1,
					   const NxVec3& color,
					   float lineWidth = 2.0f );

		void drawTriangle( const NxVec3& p0,
						   const NxVec3& p1,
						   const NxVec3& p2,
						   const NxVec3& color );

		void drawCircle( NxU32 nbSegments,
						 const NxMat34& matrix,
						 const NxVec3& color,
						 const NxF32 radius,
						 const bool semicircle = false );

		void drawEllipse( NxU32 nbSegments,
						  const NxMat34& matrix,
						  const NxVec3& color,
						  const NxF32 radius1,
						  const NxF32 radius2,
						  const bool semicircle = false );

		void drawWirePlane( NxShape* plane, 
							const NxVec3& color );

		void drawPlane( NxShape* plane );

		void drawWireBox( NxShape* box,
						  const NxVec3& color,
						  float lineWidth = 2.0f );

		void drawWireBox( const NxBox& obb,
						  const NxVec3& color,
						  float lineWidth = 2.0f );

		void drawBox( NxShape* box,
					  const NxVec3& color = SHAPE_DEFAULT_COLOR );

		void drawWireSphere( NxShape* sphere, const NxVec3& color );
		void drawSphere( NxShape* sphere, const NxVec3& color = SHAPE_DEFAULT_COLOR );

		void drawWireCapsule( NxShape* capsule, const NxVec3& color );
		void drawWireCapsule( const NxCapsule& capsule, const NxVec3& color );
		void drawCapsule( NxShape* capsule, const NxVec3& color = SHAPE_DEFAULT_COLOR );
		void drawCapsule( const NxVec3& color, NxF32 r, NxF32 h );

		void drawWireConvex( NxShape* mesh, const NxVec3& color );
		void drawConvex( NxShape* mesh, const NxVec3& color = SHAPE_DEFAULT_COLOR );

		void drawWireMesh( NxShape* mesh, const NxVec3& color );
		void drawMesh( NxShape* mesh, const NxVec3& color = SHAPE_DEFAULT_COLOR );
		void drawWheelShape( NxShape* wheel );

		void drawArrow( const NxVec3& posA,
						const NxVec3& posB,
						const NxVec3& color );

		void drawContactPoint( const NxVec3& pos,
							   const NxReal radius,
							   const NxVec3& color );

		void drawWireShape( NxShape* shape, const NxVec3& color );
		void drawShape( NxShape* shape, const NxVec3& color = SHAPE_DEFAULT_COLOR );
		void drawActor( NxActor* actor, NxActor* gSelectedActor = NULL );

		void drawTriangleList( int iTriangleCount,
		 					   Triangle *pTriangles,
							   Point *pPoints );

		void renderTerrain( const TerrainData& terrain, bool addWireframe = true );
		void renderTerrainTriangles( const TerrainData& terrain,
									 NxU32 nbTriangles,
									 const NxU32* indices );

	public:
		static const int   TERRAIN_SIZE;
		static const int   TERRAIN_NB_VERTS;
		static const int   TERRAIN_NB_FACES;
		static const float TERRAIN_OFFSET;
		static const float TERRAIN_WIDTH;
		static const float TERRAIN_CHAOS;
	};

}

#endif // __ZGLOR_OBJECT_RENDERER_H__